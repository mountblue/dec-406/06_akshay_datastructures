function spiralPrint(str, row, col) {
    var str = str.split(" ")
    var arr = new Array(row);
    var newArr = [];
    var index = 0;
    for (let i = 0; i < row; i++) {
        arr[i] = str.slice(index, index + col);
        index += col;
    }
    for (let k = 0; k <= col*2-1; k++) {
        newArr.push(arr.shift());
        if(arr.length === 0){
            break;
        }
        dc = arr[0].length;
        dr = arr.length;
        arr = rotate(dr, dc);
        function rotate(dr, dc) {
            var rotatedArr = [];
            for (let i = dc - 1; i >= 0; i--) {
                var local = [];
                for (let j = 0; j < dr; j++) {
                    local.push(arr[j][i]);
                }
                rotatedArr.push(local)
            }
            return rotatedArr;
        }
    }
    newArr.push(arr);
    console.log(newArr);
    return newArr.join(",");
}
spiralPrint("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25", 5, 5);