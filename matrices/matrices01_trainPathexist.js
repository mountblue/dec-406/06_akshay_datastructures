function trainPath(n,m,s) {
    s = s.split('');
	var row,col;
	var flag = 0;
    for(i = 0; i < n; i++){
        for(j= 0; j < m; j++){
				row=i;
				col=j;
            for(k = 0; k < s.length; k++){
              
                if(s[k] == 'L'){
                    col -= 1;
                }else if(s[k] == 'U'){
                    row -= 1;
                }else if(s[k] == 'R'){
                    col += 1;
                }else if(s[k] == 'D'){
                    row += 1;
                }
                if (row < 0 || row >= n || col < 0 || col >= m){
                    break;
                }
				if (k === s.length-1){flag = 1;} 
            }
        }
      
    }
    return flag;
}
trainPath(2,3,"LLRU");