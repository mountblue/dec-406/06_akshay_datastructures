function sudokuValidOrNot(str,n) {
    var str = str.split(" ")
    var arr= new Array(n);
    var index = 0;
    for(let i = 0; i< n; i++){
        arr[i] = str.slice(index,index+n);
        index+=n;
    }
    console.log(arr)
    for (let i = 0; i < n; i++) {
        for(let j = 0; j < n; j++){
            if(check(arr[j][i]) || check(arr[i][j]) || checkGrid(arr,i-i%3,j-j%3)){
                return 0;
            }
        }
        
    }
    return 1;
}
function check(element){
    let counts = [];
    
    for(let i = 0; i <= element.length; i++) {
        if(element[i] != 0){
        if(counts[element[i]] === undefined) {
            counts[element[i]] = 1;
        } else {
            return true;
        }
    }}
    return false;
}
function checkGrid(arr,startRow,startCol) {
        var gridArray = [];
        var curr = 0;
        for (var k = 0; k < 3; k++) { 
            for (var l = 0; l < 3; l++) { 
                curr = arr[k + startRow][l + startCol];
                gridArray.push(curr);
                if(check(gridArray)){
                    return true;
                } 
        }
        
    }
}
sudokuValidOrNot("3 0 6 5 0 8 4 0 0 5 2 8 0 0 0 0 0 0 0 8 7 0 0 0 0 3 1 0 0 3 0 1 0 0 8 0 9 0 0 8 6 3 0 0 5 0 5 0 0 9 0 6 0 0 1 3 0 0 0 0 2 5 0 0 0 0 0 0 0 0 7 4 0 0 5 2 0 6 3 0 0",9);