let LinkedList = require('./linked_list');

function main(arr,position) {
    var list = new LinkedList();
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }  
    list.popAt(position)
    list.display();
}
main([1,2,3,4,5,6,7,8,9],2)