function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };

    this.popAt = function (index=0) {
        var currentNode = head;
        var previousNode;
        var currIndex = 0;
        if (index < 0 || index >= length) {
            return null;
        }
        if (index === 0) {
            head = currentNode.next;
        } else {
            while (currIndex < index) {
                currIndex++;
                previousNode = currentNode;
                currentNode = currentNode.next;
            }
            previousNode.next = currentNode.next;
        }
        length--;
        return currentNode.value;
    }
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.modify = function () {
        var currentNode = head;
        let lastIndex = length - 1;
        var currIndex = 0;
        while (currentNode && currIndex <= lastIndex) {
            currIndex++;
            if (currentNode.value % 2 !== 0) {
                let temp = currentNode.value;
                this.remove(currentNode.value);
                this.add(temp);
            }
            currentNode = currentNode.next;
        }
    }
    this.countOccurence = function (n) {
        var currentNode = head;
        var count = 0;
        while (currentNode) {
            if (currentNode.value === n) {
                count++;
            }
            currentNode = currentNode.next;
        }
        console.log(count);
    }
    this.sortedInsert = function(value) {
        var node = new Node(value)
        var currentNode = head;
        var previousNode;
        if(currentNode.value > value){
            node.next = currentNode;
            head = node;
        }else {
        while(currentNode.value < value){
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
        node.next = currentNode;
        previousNode.next = node;
    }}
    this.removeDuplicates = function () {
        var currentNode = head;
        var previousNode;
        var obj ={};
        while(currentNode){
            if(obj.hasOwnProperty(currentNode.value)){
                previousNode.next = currentNode.next;
            } else {
                obj[currentNode.value] = 1;
                previousNode = currentNode;
            }
            currentNode = currentNode.next;
        }
    }
}

module.exports = LinkedList;