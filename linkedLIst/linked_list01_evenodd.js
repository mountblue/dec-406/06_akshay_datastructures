let LinkedList = require('./linked_list');

function main(arr) {
    var list = new LinkedList();
    
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.modify();
    list.display();
    
}
main([17, 15, 8, 9, 2, 4, 6]);