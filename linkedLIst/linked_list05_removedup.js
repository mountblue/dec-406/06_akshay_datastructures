let LinkedList = require('./linked_list');

function main(arr) {
    var list = new LinkedList();
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    } 
    list.removeDuplicates();
    list.display(); 
}
main([1,1,5,5,16,3,4,4,16,5,6,7])