let stack = require('./stack');

function removeMid(myStack) {
    var index;
    var i = 0;
    var newStack = new stack();
    if(myStack.count % 2 !== 0){
        index = (myStack.count + 1)/2 - 1;
    }else {
        index = (myStack.count/2)
    }
    while(i < myStack.count){
        if(i == index){
            i+=1;
        }
        newStack.push(myStack.obj[i])
        i++;
    }
    return newStack;
} 
function main(arr) {
    var myStack = new stack();
    for(let i = 0; i < arr.length; i++) {
        myStack.push(arr[i]);
    }
    myStack = removeMid(myStack);
    myStack.display();
}
main([1,2,3,4,5,6,7]);