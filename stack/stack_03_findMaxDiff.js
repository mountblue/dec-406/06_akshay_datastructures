let stack = require('./stack');

function findMaxDiff(myStack) {
    var leftsmall = rightSmaller(myStack);
    var rigthsmall = leftSmaller(myStack);
    var diff = [];
    for(i = 0; i < myStack.count; i++){
        diff.push(Math.abs(leftsmall.pop()-rigthsmall.obj[i]));
    }
     console.log(diff); 
}
// Gives right smaller elements of every element.
function rightSmaller(myStack) {
    var rightStack = new stack();
    var i = myStack.count;
    while(i > 0){
        var tmp = myStack.obj[i-1];
        for(var j = i-1; j >= 0; j--){
            if(tmp > myStack.obj[j]){
                rightStack.push(myStack.obj[j]);
                break;
            }
            
            if(j === 0){
                rightStack.push(0);
            }
        }
    i--;
    }
    return rightStack;

}
// Gives left smaller elements of every element.
function leftSmaller(myStack) {
    var leftStack = new stack();
    var i = 0;
    while(i < myStack.count){
        var tmp = myStack.obj[i];
        
        for(var j = i+1; j <= myStack.count; j++){
            if(tmp > myStack.obj[j]){
                leftStack.push(myStack.obj[j]);
                break;
            }
            if(j === myStack.count){
                leftStack.push(0);
            }
        }
    i++;
    }
    return leftStack;
}
function main(arr) {
    var myStack = new stack();
    for(i = 0; i < arr.length; i++) {
        myStack.push(arr[i]);
    }
    findMaxDiff(myStack);
}
main([11,3,15,7,2,6,14]);