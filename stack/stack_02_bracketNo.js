let stack = require('./stack');

function main(arr) {
    var myStack = new stack();
    var c = 1;
    var arrNum=[];
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] === '(') {
            arrNum.push(c);
            myStack.push(c);
            c++;
        }else if (arr[i] === ')') {
            arrNum.push(myStack.pop())
        }
    }
    console.log(arrNum.join(" "));
}
main('((())(()))(');