let stack = require('./stack');

function nextGreater(myStack) {
    var newStack = new stack();
    var i = 0;
    while(i < myStack.count){
        var tmp = myStack.obj[i];
        
        for(var j = i+1; j <= myStack.count; j++){
            if(tmp < myStack.obj[j]){
                newStack.push(myStack.obj[j]);
                break;
            }
            if(j === myStack.count){
                newStack.push(-1);
            }
        }
    i++;
    }
    return newStack;
}
function main(arr) {
    var myStack = new stack();
    var result = [];
    for(i = 0; i < arr.length; i++){
        myStack.push(arr[i]);
    }
    myStack = nextGreater(myStack);
    for(i = 0; i < myStack.count; i++){
        result.push(myStack.obj[i]);
    }
    console.log(result)
}
main([7,6,9,3,4,6]);