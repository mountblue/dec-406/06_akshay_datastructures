function closestSum(arr1,arr2,x) {
    var sum = 0;
    var diff;
    var min=1000;
    var newArr=[];
    for(let i=0; i<arr1.length; i++){
        for(let j = 0; j < arr2.length; j++){
            sum = arr1[i] + arr2[j];
            diff = Math.abs(x-sum)
            min = diff < min? diff: min;
        }
 }
    for(let i=0; i<arr1.length; i++){
        for(let j = 0; j < arr2.length; j++){
            sum = arr1[i] + arr2[j];
            diff = Math.abs(x-sum);
            if(diff === min){
                newArr.push(arr1[i]);
                newArr.push(arr2[j]);
                return newArr;
            }
        }
}
}
closestSum([1,4,5,7],[10,20,30,40],50);