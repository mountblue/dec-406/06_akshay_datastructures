function pendulumRep(arr) {
    var newArr=[];
    var newArr2 = [];
    arr.sort(function(a, b){return a-b});
    for(let i=0; i < arr.length; i+=2){
        newArr.unshift(arr[i]);
        newArr.push(arr[i+1]);
    }
    newArr2 = [...newArr];
    newArr2.pop();
    return newArr.length % 2 === 0? newArr: newArr2;  
}
pendulumRep([11,12,31,14])