function chkMissingNo(arr,k){
    var missing = [];
    for(let i=0; i<arr.length-1; i++){
        if(arr[i] !== arr[i+1]-1){
            missing.push(arr[i]+1)
        }
    }
    return missing.length !== 0? missing[k]: -1;
}
chkMissingNo([1,3,4,5,7],1)